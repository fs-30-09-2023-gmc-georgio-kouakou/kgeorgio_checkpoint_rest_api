const express = require('express');
const app = express();
const cors = require("cors");
console.log("App listen at port 5000");
app.use(express.json());
app.use(cors());
const mongoose = require('mongoose');


const UserModel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
  },
});

const UserShema = mongoose.model("User", UserSchema);

module.exports = Food;

mongoose.connect(MONGO_URI, {
    dbName: DATABASE_URI,
    useNewUrlParser: true,
    useUnifiedTopology: true
}, err => err ? console.log(err) :
    console.log('Impossible to connected to Checkpoint database'));

app.get('/user', async function(req, res) {
 const user = await UserModel(req.body)
 UserModel.save();
 res.json(user);
});

app.post('/', async function(req, res) {
    const user = await UserModel.find
});

app.put('/:id', async function(req, res) {
    const id = req.params.id;
    const user = await UserModel.findByIdAndUpdate(id, req.body);
    res.json(user);
});

app.delete('/:id', async function(req, res) {
    const id = req.params.id;
    const user = await UserModel.findByIdAndDelete(id);
    res.json(user);
});