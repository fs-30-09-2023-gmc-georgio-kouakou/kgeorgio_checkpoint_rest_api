const express = require('express');
const app = express();
const cors = require("cors");
console.log("App listen at port 5000");
app.use(express.json());
app.use(cors());

const UserRoute = require('./User');

app.use('/user', UserRoute);

app.listen(5000);